public class Addition{
	public static void main(String[] args){
		System.out.println("24 and 45 concatenated: "+24+45); 
		System.out.println("24 and 45 added: "+(24+45));
		System.out.println("Quotation mark!: \"");
		System.out.println("New line...\nhere!");
		/*adds the integers because both variables in the parentheses are of the same type
		 *concatenates because string+int=string and string+int+int=string+int=string*/

	}
}
