//***********************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Judson Dunaway-Barlow
//CMPSC 111 Fall 2014
//Lab # 6
//Date: Oct 9 2014
//
//Purpose: To find bugs in and add new features to a geometry calculator.
//***********************************

public class GeometricCalculator {

    public static double calculateSphereVolume(double radius) {
        double volume;
        volume = (4/3) * (Math.PI) * radius * radius;
        return volume;
    }

    public static double calculateTriangleArea(double a, double b, double c) {
        double area;
        area = a*b/2;
        return area;
    }

    public static double calculateCylinderVolume(double radius, double height) {
        double volume;
        volume = (Math.PI) * radius * radius * height;
        return volume;
    }

    public static double calculateSphereSurfaceArea(double r){
        return 4*Math.PI*r*r;
    }

    public static double calculateCylinderSurfaceArea(double r, double h){
        return 2*Math.PI*r*h+2*Math.PI*r*r;
    }

}
