//***********************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Judson Dunaway-Barlow
//CMPSC 111 Fall 2014
//Lab # 6
//Date: Oct 9 2014
//
//Purpose: To find bugs in and add new features to a geometry calculator.
//***********************************

import java.util.Date;
import java.util.Scanner;
import java.text.DecimalFormat;

public class CommandLineGeometer {

    private enum GeometricShape {sphere, triangle, cylinder};

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        GeometricShape shape = GeometricShape.sphere;
        double radius;
        double height;
        double x,y,z;
	DecimalFormat df=new DecimalFormat("0.##");

        System.out.println("Gregory M. Kapfhammer " + new Date());
        System.out.println("Welcome to the Command Line Geometer!");
        System.out.println();

	//get radius for sphere from user
        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();

	//calculate volume for sphere and print result
        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double sphereVolume = GeometricCalculator.calculateSphereVolume(radius);
        System.out.println("The volume is equal to " + df.format(sphereVolume));
        System.out.println();

	//calculate surface area of sphere and print result
	System.out.println("Calculating the surface area of a "+shape+" with radius "+radius);
	double sphereSurfaceArea=GeometricCalculator.calculateSphereSurfaceArea(radius);
	System.out.println("The surface area is equal to "+df.format(sphereSurfaceArea));
	System.out.println();

	//change shape to triangle
        shape = GeometricShape.triangle;

	//get side lengths from user
        System.out.println("What is the length of the first side?");
        x = scan.nextDouble();

        System.out.println("What is the length of the second side?");
        y = scan.nextDouble();

        System.out.println("What is the length of the third side?");
        z = scan.nextDouble();
        System.out.println();

        //calculate area for triangle and print result
	System.out.println("Calculating the area of a " + shape+" with sides "+x+", "+y+", and "+z);
        double triangleArea = GeometricCalculator.calculateTriangleArea(x, y, z);
        System.out.println("The area is equal to " + df.format(triangleArea));
        System.out.println();

	//change shape to cylinder
        shape = GeometricShape.cylinder;

	//get radius for cylinder from user
        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();

	//get height of cylinder for user
        System.out.println("What is the height for the " + shape + "?");
        height = scan.nextDouble();
        System.out.println();

	//calculate volume for cylinder and print result
        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double cylinderVolume = GeometricCalculator.calculateCylinderVolume(radius, height);
        System.out.println("The volume is equal to " + df.format(cylinderVolume));
        System.out.println();

	//calculate surface area for cylinder and print result
	System.out.println("Calculating the surface area of a "+shape+" with radius "+radius+" and height "+height);
	double cylinderSurfaceArea=GeometricCalculator.calculateCylinderSurfaceArea(radius, height);
	System.out.println("The surface area is equal to "+df.format(cylinderSurfaceArea));
	System.out.println();

    }
}
