import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class TodoList {

    private ArrayList<TodoItem> todoItems;
    private HashMap<String,ArrayList<TodoItem>> byCategory;
    private HashMap<String,ArrayList<TodoItem>> byPriority;
    private static final String TODOFILE = "todo.txt";

    public TodoList() {
        todoItems = new ArrayList<TodoItem>();
        //create maps based on priority and category
        byCategory=new HashMap<String,ArrayList<TodoItem>>();
        byPriority=new HashMap<String,ArrayList<TodoItem>>();
    }

    public void addTodoItem(TodoItem todoItem) {
        //add items to category map
        if(byCategory.containsKey(todoItem.getCategory())){
            byCategory.get(todoItem.getCategory()).add(todoItem);
        }
        else{
            ArrayList<TodoItem> tempList=new ArrayList<TodoItem>();
            tempList.add(todoItem);
            byCategory.put(todoItem.getCategory(),tempList);
        }
        //add items to priority map
        if(byPriority.containsKey(todoItem.getPriority())){
            byPriority.get(todoItem.getPriority()).add(todoItem);
        }
        else{
            ArrayList<TodoItem> tempList=new ArrayList<TodoItem>();
            tempList.add(todoItem);
            byPriority.put(todoItem.getPriority(),tempList);
        }
        todoItems.add(todoItem);
    }

    public Iterator getTodoItems() {
        return todoItems.iterator();
    }

    public void readTodoItemsFromFile() throws IOException {
        Scanner fileScanner = new Scanner(new File(TODOFILE));
        while(fileScanner.hasNext()) {
            String todoItemLine = fileScanner.nextLine();
            Scanner todoScanner = new Scanner(todoItemLine);
            todoScanner.useDelimiter(",");
            String priority, category, task;
            priority = todoScanner.next();
            category = todoScanner.next();
            task = todoScanner.next();
            TodoItem todoItem = new TodoItem(priority, category, task);
            //todoItems.add(todoItem);
            addTodoItem(todoItem);
        }
    }

    public void markTaskAsDone(int toMarkId) {
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getId() == toMarkId) {
                todoItem.markDone();
            }
        }
    }

    public Iterator findTasksOfPriority(String requestedPriority) {
        ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>();
        // TODO: Add source code that will find and return all tasks of the requestedPriority
        /*Iterator iterator=getTodoItems();
        while(iterator.hasNext()){
            TodoItem tempItem=(TodoItem)iterator.next();
            if(tempItem.getPriority().equals(requestedPriority)){
                priorityList.add(tempItem);
            }
        }*/
        priorityList=byPriority.get(requestedPriority);
        //System.out.println(byPriority.keySet());
        //System.out.println("byPriority A length is "+byPriority.get("A").size());
        return priorityList.iterator();
    }

    public Iterator findTasksOfCategory(String requestedCategory) {
        ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>();
        // TODO: Add source code that will find and return all tasks for the requestedCategory
        /*Iterator iterator=getTodoItems();
        while(iterator.hasNext()){
            TodoItem tempItem=(TodoItem)iterator.next();
            if(tempItem.getCategory().equals(requestedCategory)){
                priorityList.add(tempItem);
            }
        }*/
        categoryList=byCategory.get(requestedCategory);
        //System.out.println(byCategory.keySet());
        //System.out.println("byCategory ' Understand' length is "+byCategory.get(" Understand").size());
        return categoryList.iterator();
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            buffer.append(iterator.next().toString());
            if(iterator.hasNext()) {
                buffer.append("\n");
            }
        }
        return buffer.toString();
    }

}
