//***********************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Judson Dunaway-Barlow
//CMPSC 111 Fall 2014
//Lab # 3
//Date: Sep 18 2014
//
//Purpose: To create a tip and bill calculator.
//***********************************

import java.util.Date;
import java.util.Scanner;
import java.text.DecimalFormat;

public class Lab3{
	public static void main(String[] args){
		//Label output with name and date:
		System.out.println("Judson Dunaway-Barlow\nLab #3\n"+new Date());

		//Variable dictionary:
		Scanner reader=new Scanner(System.in); //create input reader
		DecimalFormat df=new DecimalFormat("0.##");
		
		sop("Enter name please");
		//String name=reader.next();  //get user name input
		String name="Judson";
		sop("Welcome, "+name); //welcome user

		sop("How much?"); //ask for the cost of the bill
		float bill=reader.nextFloat(); //get cost of bill

		float tip=0; //create tip variable
		sop("What tip percentage? (0-100) or (0-1)"); //ask for tip percentage
		float tipPerc=reader.nextFloat(); //get tip percentage input
		if(tipPerc<1)
			tip=tipPerc*bill; //calculate bill if input is between 0 and 1
		else tip=(tipPerc/100)*bill; //calculate bill if input is between 0 and 100
		sop(df.format(tip)); 
		float total_bill=bill+tip; //calculate total bill
		
		sop("Original bill was: "+df.format(bill)); //print original bill amount
		sop("Tip amount is: "+df.format(tip)); //print tip amount
		sop("Total bill is: "+df.format(total_bill)); //print total bill amount

		sop("How many people are splitting the bill?");
		int numPeople=reader.nextInt(); //get input for number of people splitting the bill
		float indivShare=total_bill/numPeople; //calculate cost of the meal for each person
		sop("Each person should pay $"+df.format(indivShare)); //tell user how much each person should pay

		sop("Thanks for using the bill assister!"); //thank user
	}

	public static void sop(Object o){
		System.out.println(o);
	}
}
