//***********************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Judson Dunaway-Barlow
//CMPSC 11 Fall 2014
//Lab # 2
//Date: SEP 11 2014
//
//Purpose: To calculate a random batting average, play a game of FizzBuzz, and 
//***********************************

import java.util.Date;
import java.lang.Math;
import java.util.Scanner;

public class Lab2{
	public static void main(String[] args){
		//Label output with name and date:
		System.out.println("Judson Dunaway-Barlow\nLab #2\n"+new Date());

		//Variable dictionary:

		//Calculate a random Batting Average
		float hits=(float)(int)(Math.random()*250); //gets random # of hits from 0-250
		sop("Hits: "+(int)hits);
		float atBats=hits+(int)(Math.random()*200); //gets random # of at bats from hits to hits+200

		sop("At Bats: "+(int)atBats);
		float battingAvg=hits/atBats; //calculates batting average
		sop("Batting Average: "+battingAvg);
		
		//Play FizzBuzz
		String fizzGame="";
		for(int i=1;i<25;i++){
			if(i%7==0)
				fizzGame+="fizzbuzz\n"; //if the number is divisible, add "fizzbuzz" and a line return to the string
			else fizzGame+=(i+"\n"); //otherwise, just add the number and a line return to the string
		}
		sop(fizzGame);

		sop("How many miles?");
		Scanner reader=new Scanner(System.in); //create reader for user input
		int miles=reader.nextInt(); //get user input for miles
		sop("Miles: "+miles);
		sop("At what pace? Enter as mm:ss");
		String pace=reader.next(); //get user input for pace
		sop("Pace: "+pace);
		String[] paceArray=pace.split(":"); //separate the minutes and seconds parts of the pace
		int paceM=Integer.parseInt(paceArray[0]); //create integer out of minutes part
		int paceS=Integer.parseInt(paceArray[1]); //create integer out of secondss part
		sop("Pace is "+paceM+" minutes and "+paceS+" seconds per mile.");

		int totalM=paceM*miles; //calculate total full minutes
		int totalS=paceS*miles; //calculate total seconds
		int actualM=totalM+totalS/60; //get number of full minutes out of the total seconds
		int actualS=totalS%60; //get remaining seconds

		sop("Your run should take "+actualM+" minutes and "+actualS+" seconds ("+actualM+":"+actualS+").");
	}

	public static void sop(Object o){ //make it easier/quicker to print
		System.out.println(o);
	}
}
