//***********************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Judson Dunaway-Barlow
//CMPSC 111 Fall 2014
//Lab # 4
//Date: Sep 28 2014
//
//Purpose: To draw a picture on the screen using the Graphics class.
//***********************************
import java.awt.*;
import javax.swing.JApplet;
import java.util.Date;

public class Lab4 extends JApplet{
	//-------------------------------------------------
	// Use Graphics methods to add content to the drawing canvas
	//-------------------------------------------------
	public void paint(Graphics page){
		int monitorX=250;
		int monitorY=50;
		int monitorW=100;
		int monitorH=100;

		int keyboardX=monitorX-25;
		int keyboardY=monitorY+monitorH+50;
		int keyboardW=monitorW+50;
		int keyboardH=40;

		page.setColor(new Color(128,128,128)); //gray for background
		page.fillRect(0,0,600,400); //draw background [Object 1] [Object Type 1]
		
		page.setColor(Color.black); //black for monitor
		page.fillRect(monitorX,monitorY,monitorW,monitorH); //draw monitor [Object 2]
		
		page.setColor(Color.white); //change to white for screen
		page.fillRect(monitorX+5,monitorY+5,(int)(monitorW*0.9),(int)(monitorH*0.9)); //draw screen [Object 3]

		page.setColor(Color.black); //black for monitor stand
		page.drawLine(monitorX+35,monitorY+monitorH,monitorX+25,monitorY+monitorH+25); //left side of monitor stand [Object 4] [Object Type 2]

		page.drawLine(monitorX+monitorW-35,monitorY+monitorH,monitorX+monitorW-25,monitorY+monitorH+25); //right side of monitor stand [Object 5]
		page.drawLine(monitorX+25,monitorY+monitorH+25,monitorX+monitorW-25,monitorY+monitorH+25); //left side of monitor stand [Object 6]

		page.setColor(new Color(128,255,128)); //green for keyboard body
		page.fillRect(keyboardX,keyboardY,keyboardW,keyboardH); //draw keyboard [Object 7]
		page.setColor(Color.black); //black for keyboard outline
		page.drawRect(keyboardX,keyboardY,keyboardW,keyboardH); //draw keyboard outline [Object 8] [Object Type 3]


		int mouseX=keyboardX+keyboardW+30;
		int mouseY=keyboardY;
		int mouseW=30;
		int mouseH=37;
		page.setColor(new Color(200,200,200)); //lighter gray for mouse
		page.fillOval(mouseX,mouseY,mouseW,mouseH); //draw mouse [Object 9] [Object Type 4]

		page.setColor(Color.black);
		for(int i=10;i<keyboardH;i+=10){
			page.drawLine(keyboardX,keyboardY+i,keyboardX+100,keyboardY+i); //draw keyboard horizontal lines [Objects 10-12]
		}
		for(int i=5;i<=100;i+=5){
			page.drawLine(keyboardX+i,keyboardY,keyboardX+i,keyboardY+keyboardH); // draw keyboard vertical lines[Objects 13-32]
		}

		int numpadX=keyboardX+130;
		int numpadY=keyboardY+7;
		int numpadW=15;

		page.drawRect(numpadX,numpadY,numpadW,keyboardH-15); //draw numpad outline [Object 33]

		for(int i=0;i<24;i+=25/3){
			page.drawLine(numpadX,numpadY+i,numpadX+numpadW,numpadY+i); //draw horizontal lines on keypad [Objects 34-35]
		}

		for(int i=0;i<14;i+=15/3){
			page.drawLine(numpadX+i,numpadY,numpadX+i,numpadY+keyboardH-15); //draw horizontal lines on keypad [Objects 36-37]

		}

		page.drawLine(mouseX+(int)(0.5*mouseW),mouseY,mouseX+(int)(0.5*mouseW),mouseY+(int)(0.5*mouseW)); //draw mouse button divider [Object 38]
		
		page.setColor(Color.yellow);
		page.setColor(new Color(0,255,0));
		page.fillRect(100,100,100,100);
	}
}
