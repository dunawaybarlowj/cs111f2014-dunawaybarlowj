//***********************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Judson Dunaway-Barlow
//CMPSC 111 Fall 2014
//Lab # 8
//Date: Nov 04 2014
//
//Purpose: To write a program that checks a 4x4 sudoku grid.
//***********************************

import java.util.Scanner;

public class SudokuChecker{
    private int w1,w2,w3,w4,x1,x2,x3,x4,y1,y2,y3,y4,z1,z2,z3,z4;
    private Scanner reader;

    public SudokuChecker(){
        //initialize
        reader=new Scanner(System.in);
        w1=0;
        w2=0;
        w3=0;
        w4=0;
        x1=0;
        x2=0;
        x3=0;
        x4=0;
        y1=0;
        y2=0;
        y3=0;
        y4=0;
        z1=0;
        z2=0;
        z3=0;
        z4=0;
    }

    public void getGrid(){
        sop("Please input lines, separating each number on one line with spaces.\n");
        System.out.print("Input Line 1: ");
        String[] line1=reader.nextLine().split(" ");
        w1=i(line1[0]);
        w2=i(line1[1]);
        w3=i(line1[2]);
        w4=i(line1[3]);

        System.out.print("Input Line 2: ");
        String[] line2=reader.nextLine().split(" ");
        x1=i(line2[0]);
        x2=i(line2[1]);
        x3=i(line2[2]);
        x4=i(line2[3]);

        System.out.print("Input Line 3: ");
        String[] line3=reader.nextLine().split(" ");
        y1=i(line3[0]);
        y2=i(line3[1]);
        y3=i(line3[2]);
        y4=i(line3[3]);

        System.out.print("Input Line 4: ");
        String[] line4=reader.nextLine().split(" ");
        z1=i(line4[0]);
        z2=i(line4[1]);
        z3=i(line4[2]);
        z4=i(line4[3]);
    }

    public void checkGrid(){
        boolean valid=true;
        //lines
        if(w1+w2+w3+w4!=10){
            sop("Grid is invalid because row 1 doesn't work");
            valid=false;
        }

        if(x1+x2+x3+x4!=10){
            sop("Grid is invalid because row 2 doesn't work");
            valid=false;
        }

        if(y1+y2+y3+y4!=10){
            sop("Grid is invalid because row 3 doesn't work");
            valid=false;
        }

        if(z1+z2+z3+z4!=10){
            sop("Grid is invalid because row 4 doesn't work");
            valid=false;
        }

        //columns
        if(w1+x1+y1+z1!=10){
            sop("Grid is invalid because column 1 doesn't work");
            valid=false;
        }
        if(w2+x2+y2+z2!=10){
            sop("Grid is invalid because column 2 doesn't work");
            valid=false;
        }
        if(w3+x3+y3+z3!=10){
            sop("Grid is invalid because column 3 doesn't work");
            valid=false;
        }

        if(w4+x4+y4+z4!=10){
            sop("Grid is invalid because column 4 doesn't work");
            valid=false;
        }


        //groups
        if(w1+w2+x1+x2!=10){
            sop("Grid is invalid because group 1 doesn't work");
            valid=false;
        }

        if(y1+y2+z1+z2!=10){
            sop("Grid is invalid because group 2 doesn't work");
            valid=false;
        }

        if(w3+w4+x3+x4!=10){
            sop("Grid is invalid because group 3 doesn't work");
            valid=false;
        }

        if(y3+y4+z3+z4!=10){
            sop("Grid is invalid because group 4 doesn't work");
            valid=false;
        }

        if(valid)
            sop("Sudoku Grid is valid!");
    }

    public void sop(Object o){ //makes System.out.print quicker for me to use
        System.out.println(o);
    }

    public int i(String s){
        return Integer.parseInt(s);
    }
}
