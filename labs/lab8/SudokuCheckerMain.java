//***********************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Judson Dunaway-Barlow
//CMPSC 111 Fall 2014
//Lab # 8
//Date: Oct 30 2014
//
//Purpose: To write a program that checks a 4x4 sudoku grid.
//***********************************

import java.util.Date;

public class SudokuCheckerMain{
	public static void main(String[] args){
		//Label output with name and date:
		System.out.println("Judson Dunaway-Barlow\nLab #8\n"+new Date());

		//Variable dictionary:
        SudokuChecker checker=new SudokuChecker();
        sop("Welcome to the Sudoku Checker program!");
        checker.getGrid();
        checker.checkGrid();
	}

	public static void sop(Object o){ //makes System.out.println quicker for me to use
		System.out.println(o);
	}
}
