//***********************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Judson Dunaway-Barlow
//CMPSC 111 Fall 2014
//Lab # 7
//Date: Oct 23 2014
//
//Purpose: To draw a picture using the Graphics class in self-created methods.
//***********************************
import java.awt.*;
import javax.swing.JApplet;
import java.util.Date;

public class Lab7 extends JApplet{
	//-------------------------------------------------
	// Use Graphics methods to add content to the drawing canvas
	//-------------------------------------------------
	public void paint(Graphics page){

        Lab7Drawing drawing=new Lab7Drawing(page, new Color(128,128,128));
		int monitorX=250;
		int monitorY=50;
		int monitorW=100;
		int monitorH=100;

		int keyboardX=monitorX-25;
		int keyboardY=monitorY+monitorH+50;
		int keyboardW=monitorW+50;
		int keyboardH=40;

        int mouseX=keyboardX+keyboardW+30;
		int mouseY=keyboardY;
		int mouseW=30;
		int mouseH=37;

        int numpadX=keyboardX+130;
		int numpadY=keyboardY+7;
		int numpadW=15;
        int numpadH=keyboardH-15;

        drawing.drawMonitor(monitorX,monitorY,monitorW,monitorH);
        drawing.drawKeyboard(monitorX-25, monitorY+monitorH+50, monitorW+50, 40, numpadX, numpadY, numpadW, numpadH);
        drawing.drawMouse(mouseX, mouseY, mouseW, mouseH);
	}
}
