//***********************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Judson Dunaway-Barlow
//CMPSC 111 Fall 2014
//Lab #7
//Date: Oct 23 2014
//
//Purpose: To draw a picture using the Graphics class in self-created methods.
//***********************************

import java.awt.*;

public class Lab7Drawing{

    Color backgroundColor;
    Graphics page;

	public Lab7Drawing(Graphics p, Color bg){
		page=p;
        backgroundColor=bg;
        page.setColor(backgroundColor);
        page.fillRect(0,0,600,400); //draw background
	}

    public void drawMonitor(int monitorX, int monitorY, int monitorW, int monitorH){
        page.setColor(Color.black); //black for monitor
		page.fillRect(monitorX,monitorY,monitorW,monitorH); //draw monitor

		page.setColor(Color.white); //change to white for screen
		page.fillRect(monitorX+5,monitorY+5,(int)(monitorW*0.9),(int)(monitorH*0.9)); //draw screen

		page.setColor(Color.black); //black for monitor stand
		page.drawLine(monitorX+35,monitorY+monitorH,monitorX+25,monitorY+monitorH+25); //left side of monitor stand

		page.drawLine(monitorX+monitorW-35,monitorY+monitorH,monitorX+monitorW-25,monitorY+monitorH+25); //right side of monitor stand
		page.drawLine(monitorX+25,monitorY+monitorH+25,monitorX+monitorW-25,monitorY+monitorH+25); //left side of monitor stand
    }

    public void drawKeyboard(int keyboardX, int keyboardY, int keyboardW, int keyboardH, int numpadX, int numpadY, int numpadW, int numpadH){
        page.setColor(new Color(128,255,128)); //green for keyboard body
		page.fillRect(keyboardX,keyboardY,keyboardW,keyboardH); //draw keyboard
		page.setColor(Color.black); //black for keyboard outline
		page.drawRect(keyboardX,keyboardY,keyboardW,keyboardH); //draw keyboard outline
        page.setColor(Color.black);
		for(int i=10;i<keyboardH;i+=10){
			page.drawLine(keyboardX,keyboardY+i,keyboardX+100,keyboardY+i); //draw keyboard horizontal lines
		}
        page.setColor(Color.black);
		for(int i=5;i<=100;i+=5){
			page.drawLine(keyboardX+i,keyboardY,keyboardX+i,keyboardY+keyboardH); // draw keyboard vertical lines
		}
        page.drawRect(numpadX,numpadY,numpadW, numpadH); //draw numpad outline

		for(int i=0;i<24;i+=25/3){
			page.drawLine(numpadX,numpadY+i,numpadX+numpadW,numpadY+i); //draw horizontal lines on keypad
		}

		for(int i=0;i<14;i+=15/3){
			page.drawLine(numpadX+i,numpadY,numpadX+i,numpadY+keyboardH-15); //draw horizontal lines on keypad

		}
    }

    public void drawMouse(int mouseX, int mouseY, int mouseW, int mouseH){
		page.setColor(new Color(200,200,200)); //lighter gray for mouse
		page.fillOval(mouseX,mouseY,mouseW,mouseH); //draw mouse
        page.setColor(Color.black);
        page.drawLine(mouseX+(int)(0.5*mouseW),mouseY,mouseX+(int)(0.5*mouseW),mouseY+(int)(0.5*mouseW)); //draw mouse button divider
    }

	public static void sop(Object o){ //makes System.out.println quicker for me to use
		System.out.println(o);
	}
}
