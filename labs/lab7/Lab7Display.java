//***********************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Judson Dunaway-Barlow
//CMPSC 111 Fall 2014
//Lab # 7
//Date: Oct 23 2014
//
//Purpose: To draw a picture using the Graphics class in self-created methods.
//***********************************

import javax.swing.*;

public class Lab7Display{
    public static void main(String[] args){
        	JFrame window = new JFrame(" Judson Dunaway-Barlow ");
        // Add the drawing canvas and do necessary things to
        // make the window appear on the screen!
    	window.getContentPane().add(new Lab7());
    	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);
		window.setSize(600, 400);

    }
}
