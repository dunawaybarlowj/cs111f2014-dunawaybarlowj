//***********************************
//Honor Code: The work we are submitting is a result of our own thinking and efforts.
//Judson Dunaway-Barlow & Luke Regan
//CMPSC 111 Fall 2014
//Lab # 5 
//Date: Oct 06 2014
//
//Purpose: To write a Java program that manipulates strings of DNA by appropriately using methods from the String and Random classes.
//***********************************

import java.util.Date;
import java.util.Scanner;
import java.util.Random; //so much cooler and easier than Math.random()!

public class Lab5{
	public static void main(String[] args){
		//Label output with name and date:
		System.out.println("Judson Dunaway-Barlow and Luke Regan\nLab #5\n"+new Date());

		//Variable dictionary:
		Scanner reader=new Scanner(System.in);
		Random rand=new Random();
		String options="ATCG";
		String dnaString="";
		
		//Step 1: Get user input
		while(!isValid(dnaString)){
			System.out.print("Enter a strand of DNA: ");
			dnaString=reader.nextLine();
		}

		//Step 2: Print complement
		dnaString=dnaString.toUpperCase();
		sop("dnaString is: "+dnaString);
		String comp=complement(dnaString);
		sop("Complement is: "+comp);

		//Step 3: Insert random letter
		int letterInt = rand.nextInt(4);
		char letterToAdd=options.charAt(letterInt);
		int randIndex=rand.nextInt(dnaString.length());
		sop("Adding "+letterToAdd+" at index "+randIndex+" gives "+dnaString.substring(0,randIndex)+letterToAdd+dnaString.substring(randIndex));

		//Step 4: Delete random letter
		int indexToDelete=rand.nextInt(dnaString.length());
		sop(indexToDelete);
		if(dnaString.length()!=0){
			sop("Deleting index "+indexToDelete+" gives "+dnaString.substring(0,indexToDelete-1)+dnaString.substring(indexToDelete));
		}
		else sop("Deleting index 0 deletes the entire string!");
		
		//Step 5: Change random letter
		letterInt=rand.nextInt(4);
		char letterToChangeTo=options.charAt(letterInt);
		int indexToChange=rand.nextInt(dnaString.length());
		//sop (line doesn't work)
		sop("Changing letter "+dnaString.charAt(indexToChange)+" at index "+indexToChange+" to "+letterToChangeTo+" gives "+dnaString.substring(0,indexToChange)+letterToChangeTo+dnaString.substring(indexToChange+1));
	}

	//makes System.out.println quicker for me to use
	public static void sop(Object o){
		System.out.println(o);
	}

	//Returns the complement of a string of DNA
	public static String complement(String s){
		s=s.toUpperCase();
		s=s.replace('T','a');
		s=s.replace('A','T');
		s=s.replace('G','c');
		s=s.replace('C','G');
		return s.toUpperCase();
	}
	
	//Checks if a given string consists only of a, c, t, and g
	public static boolean isValid(String s){
		s=s.toUpperCase();
		if(s.equals(""))
			return false;
		for(int i=0;i<s.length();i++){
			if(!(s.charAt(i)=='A'||s.charAt(i)=='C'||s.charAt(i)=='T'||s.charAt(i)=='G')){
				return false;
			}
		}
		return true;
	}
}
