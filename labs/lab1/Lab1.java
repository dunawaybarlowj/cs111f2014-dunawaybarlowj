//***************************************
// Judson Dunaway-Barlow
// CMPSC 111
// 4 September 2014
// Lab 1
//
//Listing 1.1 from Lewis & Loftus, slightly modified
//Demonstrates the basic structure of a Java application
//***************************************

import java.util.Date;

public class Lab1{
	public static void main(String[] args){
		System.out.println("Judson Dunaway-Barlow "+new Date());
		System.out.println("Lab 1");
		System.out.println("Advice from James Gosling, creator of Java:");
		System.out.println("Don't be intimidated--give it a try!");
	}
}
