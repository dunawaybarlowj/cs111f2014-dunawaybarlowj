//***************************************
// Judson Dunaway-Barlow
// CMPSC 111
// 4 September 2014
// Lab 1 Part 2
//
//Listing 1.1 from Lewis & Loftus, slightly modified
//Demonstrates the basic structure of a Java application
//***************************************

import java.util.Date;

public class Lab1Part2{
	public static void main(String[] args){
		System.out.println("Judson Dunaway-Barlow "+new Date());
		System.out.println("Lab 1 Part 2");
		System.out.println("I am creating a new line");
		System.out.println("And yet another line");
		System.out.println("Here is a random number from 0-10: "+(Math.random()*10));
	}
}
