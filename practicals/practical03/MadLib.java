//***********************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Judson Dunaway-Barlow
//CMPSC 11 Fall 2014
//Lab # 
//Date: Sep 18 2014
//
//Purpose: 
//***********************************

import java.util.Date;
import java.util.Scanner;

public class MadLib{
	public static void main(String[] args){
		sop("Judson Dunaway-Barlow, CMPSC 111\n"+new Date());
		
		Scanner reader=new Scanner(System.in);

		sop("Clothing (singular):");
		String clothing=reader.next();

		sop("Adjective:");
		String adjective1=reader.next();

		sop("Liquid:");
		String liquid=reader.next();

		sop("Verb (infinitive):");
		String verb1=reader.next();
		
		sop("Teacher:");
		String teacher=reader.next();
		
		sop("Your School:");
		String school=reader.next();

		sop("Plural noun:");
		String noun1=reader.next();
		
		sop("Present tense verb:");
		String verb2=reader.next();
		
		sop("Singular noun:");
		String noun2=reader.next();
		
		sop("Adjective:");
		String adjective2=reader.next();

		sop("Singular noun:");
		String noun3=reader.next();

		sop("Musical instrument (singular):");
		String instrument=reader.next();
		

		
		
		
		sop("Here is a great way to spend a winter day. First, put on a nice warm "+clothing+".");
		sop("You want to be sure you stay warm and "+adjective1+".");
		sop("Next, fill a thermos full of hot steamy "+liquid+".");
		sop("Now you are ready to go snow "+verb1+"!");
		sop("Some people say only experts should go snow "+verb1+", but I think anyone can do it if you use a little common sense.");
		sop("I have heard a rumor that "+teacher+" will be offering special snow "+verb1+" classes after school at "+school+", but I don't know if that's true.");
		sop("The best place to go snow "+verb1+" is on a hill that is covered with snow.");
		sop("First you will want to clear away any leaves and "+noun1+".");
		sop("Stand at the top of the hill and shout, \"Hey, everybody, "+verb2+" out below!\"");
		sop("Then step onto a(n) "+noun2+", point yourself downhill, and off you go.");
		sop("Snow "+noun1+" is fun, healthy, and "+adjective2+".");
		sop("And here is one more tip: everyone will be especially impressed if you can balance a(n) "+noun3+" on your head or play a(n) "+instrument+" as you sail by.");
	}

	public static void sop(Object o){
		System.out.println(o);
	}
}
