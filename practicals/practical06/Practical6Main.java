//*****************************
// CMPSC 111
// Practical 6
// October 29, 2014
//
// Purpose: a program that determines what activities happen
// during a specific year.
//*****************************

import java.util.Date;
import java.util.Scanner;
public class Practical6Main
{
    public static void main ( String[] args)
    {
        //Variable dictionary
        Scanner scan = new Scanner(System.in);
        int userInput;

        System.out.println("Please enter a year between 1000 and 3000!");
        userInput = scan.nextInt();

        YearChecker activities = new YearChecker(userInput);

        // TO DO: include method calls

        if(activities.isLeapYear())
            sop(userInput+" is a leap year.");
        else sop(userInput+" is not a leap year.");

        if(activities.isCicadaYear())
            sop("It's a cicada year.");
        else sop("It's not a cicada year.");

        if(activities.isSunspotYear())
            sop("It's a sunspot year.");
        else sop("It's not a sunspot year.");

        System.out.println("Thank you for using this program.");
    }

    public static void sop(Object o){
        System.out.println(o);
    }
}
