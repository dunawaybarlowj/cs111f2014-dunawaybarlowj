//*****************************
// CMPSC 111
// Practical 6
// October 29, 2014
//
// Purpose: determine if user's year input is a leap year,
// cicada brood II emergence year, or a peak sunspot year.
//*****************************

import java.util.Scanner;

public class YearChecker
{
    //Create instance variables
    int year;

    //Create a constructor
    public YearChecker(int y)
    {
        year = y;
    }

    // a method that checks if the user's input year is a leap year
    public boolean isLeapYear()
    {
        // TO DO: complete
        return (year%400==0||(year%100!=0&&year%4==0));
    }

    // a method that checks if the user's input year is a cicada year
    public boolean isCicadaYear()
    {
        // TO DO: complete
        return (2013-year)%17==0;
    }

    // a method that check if the user's input year is a sunspot year
    public boolean isSunspotYear()
    {
        // TO DO: complete
        return (2013-year)%11==0;
    }
}
