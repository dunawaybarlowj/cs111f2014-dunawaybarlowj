import java.util.Date;
import java.util.Random;
import java.util.Scanner;

public class GuessingGame{
    public static void main(String[] args){
        sop("Judson Dunaway-Barlow");
        sop("Practical 7");
        sop(new Date());
        int guess=0;
        int tries=0;
        Random rand=new Random();
        Scanner reader=new Scanner(System.in);
        int randNum=rand.nextInt(100);
        sop(randNum);
        while(guess!=randNum){
            tries++;
            sop("Guess a number!");
            guess=reader.nextInt();
            if(guess>randNum)
                sop("Too high!");
            else if(guess<randNum)
                sop("Too low!");
        }
        sop("You got it in "+tries+" tries!");
    }

    public static void sop(Object o){
        System.out.println(o);
    }
}
