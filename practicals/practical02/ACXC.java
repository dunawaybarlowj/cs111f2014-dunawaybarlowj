//***********************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Judson Dunaway-Barlow
//CMPSC 11 Fall 2014
//Lab # 
//Date: mmm dd 2014
//
//Purpose: 
//***********************************

import java.util.Date;

public class ACXC{
	public static void main(String[] args){
		sop("Judson Dunaway-Barlow, CMPSC 111\n"+new Date());
		sop("      /\\        -------     \\    /    -------");
		sop("     /  \\       |            \\  /     |");
		sop("    /    \\      |             \\/      |");
		sop("   /------\\     |             /\\      |");
		sop("  /        \\    |            /  \\     |");
		sop(" /          \\   |           /    \\    |");
		sop("                -------               -------");

	}

	public static void sop(Object o){
		System.out.println(o);
	}
}
